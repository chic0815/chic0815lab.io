# 🌵 How To Use GitHub?

## Create Repository

Press `+` button in the upper right corner.

Click `Create a new repository`

> **Note**: Whenever you can change the settings.

## Set Up in Desktop

Create git account

`git config --global user.name jaesung  // replace "jaesung" with your name`
`git config --global user.email jaesung@sweetlab.com // replace "jaesung@sweetlab.com" with your email`

Clone repository URL you made before

`cd /Users/jaesung/Desktop/MyGit     // Move to folder in which you want to save cloned git`
`git clone https://github.com/airbnb/swift.git   // clone!!!`

## Drag Your Project

> **Important**: If your project has .gitignore secret file, you must remove it.

Drag your local project and drop to the git folder.

## Commit! Push!

To save your git, you need to do “commit”

`git commit -m "Initial Commit"`

However the saved git is on local(your own computer!). You need to upload it on the repository(= remote) created at **GitHub** or **GitLab**.

To upload your git, you need to do ”push”

```
git push origin master`
// push your local git to remote branch named "master"

// or

git push origin feature/jaesung/someBranch 
// push your local git to remote branch named "feature/jaesung/someBranch"
```

