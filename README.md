# J A E S U N G

- - -

## Welcome to my page 🥳

### My profile 🧐

> Electrical Engineering, Konkuk University, current

>  WWDC 19 Scholarship Winner

> Software engineer intern (Voice & Video), SendBird


### Contact

**Email**: chic0815@icloud.com

**Instagram**: https://www.instagram.com/let_jaesung_go





© 2019 Jaesung


- - -
[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
