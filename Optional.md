# ⁉️ Watch out Optional
> **중요**: 스크롤을 내리면 한국어도 있습니다.

## Remember that Optional makes new type.
`Int` and `Int?` are different **type**
`String` and `String?` are different **type**
Like `Int` and `String` are different, think upper types differently.

## Do not use force casting except for `@IBOutlet`.

### Force casting
`var optionalValue: String?`
`let value: String = optionalValue!`

If the optional value is `nil`, force casting causes error.

> Let's fill out below code.
> ```Swift
>   authenticate(with: "USER_ID") { error in    // error: Error?
>       // what to do? Let's fill out!
>   }
>````

## `== nil`
If you only want to check wether it is `nil`, use `== nil`

```Swift
authenticate(with: "USER_ID") { error in
    if error == nil {
        print("Authenticated Successfully!")
    } 
}
```

## `if let ~`
If you want to use the optional value when it is not `nil`, use `if let`

```Swift
authenticate(with: "USER_ID") { error in
    if let error = error {
        print(error.localDescription)
    } 
}
```


## `guard let ~ else { return }`
If you want to return directly when the optional value doesn't satisfy the condition, use `guard let ~ else { return }`
I like this phase. This makes a golden path.

```Swift
authenticate(with: "USER_ID") { error in
    guard let error = error else {
        print("Authenticated Successfully!")
        return
    } 
    print(error.localDescription)
}
```

- - -

## 옵셔널은 새로운 타입을 제공한다는 것을 기억하세요.
`Int` 과 `Int?` 은 다른 **타입** 입니다.
`String` 과 `String?` 역시 다른 **타입**입니다.
`Int`와 `String`이 서로 다른 타입인 것 처럼, 옵셔널은 새로운 타입임을 명심하세요.

## `@IBOutlet`을 제외하고 Force casting을 사용하지 않도록 합니다..

### Force casting
옵셔널 값에 `!`를 붙이는 방법
`var optionalValue: String?`
`let value: String = optionalValue!`

만약 옵셔널 값이 `nil`이라면 이는 에러를 야기합니다.

> 그럼 아래의 코드를 함께 채워보도록 하겠습니다.
> ```Swift
>   authenticate(with: "USER_ID") { error in    // error: Error?
>       // 여기를 채우겠습니다 :)
>   }
>````

## `== nil`
`nil`인지 여부만 체크하고 싶다면 `== nil`를 사용하세요.

```Swift
authenticate(with: "USER_ID") { error in
    if error == nil {
        print("Authenticated Successfully!")
    } 
}
```

## `if let ~`
옵셔널 값이 `nil`이 아닐 때 그 값을 사용하고 싶다면 `if let`를 사용하세요.

```Swift
authenticate(with: "USER_ID") { error in
    if let error = error {
        print(error.localDescription)
    } 
}
```


## `guard ~ else { return }`
옵셔널 값이 조건을 만족시키지 못할 때 바로 `return`하고 싶다면 `guard ~ else { return }`를 사용하세요.
이에 더해서 옵셔널 값이 `nil`이 아닐 때 그 값을 사용하고 싶다면 `guard let ~ else { return }` 구문을 사용하세요.
이 구문은 황금길을 제공하기 때문에 개인적으로 좋아합니다.

```Swift
authenticate(with: "USER_ID") { error in
    guard let error = error else {
        print("Authenticated Successfully!")
        return
    } 
    print(error.localDescription)
}
```
