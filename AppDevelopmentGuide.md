> This page is for App Development Guide

# App Development Guide

## 1. App Name

### Naming it as a word or two words with no space.

> **Why?** If the name is longer than 2 words, it is hard to remember the name. Also, iPhone shows less than 10 characters on the home screen.

Name it with a word or two words with no space makes clarify and implicitly what the app is about.

### Do not use combined  / mixed word

> **Why?** It is hard to understand what this app is about. Also, it can look weird.

For example, Jumfly (jump + fly)

## 2. Copyright

“©” + “year” + “team name”

> © 2020 SweetLab. All Rights Reserved.

If the app is started with personal project, add your name in front of team name.

“©” + “year” + “name” + “team name”

> © 2020 Jaesung & SweetLab. All Rights Reserved.

## 3. Privacy Policy

### Do not collect meaningless users' information.

#### If the app service is limited in

> **South Korea** , Privacy Policy in **Korean** must be required.

> **USA** or **Canada** or **UK**, Privacy Policy in **English** must be required.

> **Brazil**, Privacy Policy in **Portuguese** must be required.

> **Spain** or **South America** except for Brazil, Privacy Policy in **Spanish** must be required.

> **Germany**, Privacy Policy in **German** must be required.

> **France**, Privacy Policy in **French** must be required.

### Implicit why we collect user personal information and where we use.

### Use GitLab or Github when the website link is required.

## 4. Advertisement, In-App-Purchase

### No advertisement

> **Why?** For the best User Experience, we do NOT provide advertisement on our apps.

### Be careful when you try to apply for IAP function.

> **Why?** IAP is helpful for our profit, but it can allow to provide worse User Experience to users.

## 5. Supporting Platform

### Reference

#### iOS

> **AR**: 11.0 or later

> **Reality AR**: 13.0 or later

> **Multiple Scene**: 13.0 or later

> **iPadOS**: 13.0 or later

#### watchOS

> **Independent app**: 6.0 or later

#### macOS

> **SwiftUI**: Catalyst

