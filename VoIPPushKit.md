# VoIP with PushKit

```Swift
import PushKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var voipRegistry: PKPushRegistry?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.voipRegistry = PKPushRegistry(queue: nil)
        self.voipRegistry?.delegate = self
        self.voipRegistry?.desiredPushTypes = [.voIP]
        return true
    }
}

extension AppDelegate: PKPushRegistryDelegate {
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        let tokenString = pushCredentials.token.map { String(format: "%02x", $0) }.joined()
        print("[token] push token is \(tokenString)")
    }
    
    // Handle Incoming pushes
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        
    }
}
```

## Info.plist

✅`background mode`

✅`Provide Voice over IP Services`


## VoIP PKPushRegistry

```Swift
self.voipRegistry = PKPushRegistry(queue: nil)
self.voipRegistry?.delegate = self
self.voipRegistry?.desiredPushTypes = [.voIP]
```


## Device Token

You can easily get your device token with below method.

```Swift
let tokenString = pushCredentials.token.map { String(format: "%02x", $0) }.joined()
```

You have to register the the device token to your own server.
